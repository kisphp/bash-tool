#!/usr/bin/env bash

mysql () {
    if [[ "${1}" == 'admin' ]]; then
        _mysql_run_admin
    else
        _mysql_run_container $@
    fi
}

_mysql_run_container () {
    if [[ "${1}" != '' ]];then
        VERSION="${1}"
    else
        VERSION="5.6"
    fi

    docker run \
        -p "3306:3306" \
        -e MYSQL_DATABASE=kisphp \
        -e MYSQL_ROOT_PASSWORD=kisphp \
        -e MYSQL_USER=kisphp \
        -e MYSQL_PASSWORD=kisphp \
        --name docker_mysql \
        -t mysql:${VERSION}
}

_mysql_run_admin () {
    docker run --rm \
        --name myadmin \
        -e PMA_HOST=docker_mysql \
        -p "8888:80" \
        --link docker_mysql:docker_mysql \
        -t phpmyadmin/phpmyadmin
}

mysql_help () {
    pre_help "mysql" "Run mysql 5.6 inside a docker container"
    pre_help "mysql admin" "Run phpmyadmin that will connect to mysql server. View at localhost:8888"
}
