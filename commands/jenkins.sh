#!/usr/bin/env bash

jenkins () {
    docker run \
        --rm \
        --name=kisphp-jenkins \
        -u root \
        -p 8080:8080 \
        -v /var/run/docker.sock:/var/run/docker.sock \
        -v "$HOME":/home \
        -t jenkinsci/blueocean
}

jenkins_help () {
    pre_help "jenkins (j)" "Run jenkins inside docker container. Available at localhost:8080"
}
