#!/usr/bin/env bash

set -e

kp_codecept () {

  DOCKER_DB_NAME='kisphp-ci-mysql'

  NETWORK=$(docker network ls | awk '$2 == "kisphp"' | awk '{print $1}')
  DATABASE_SERVICE=$(docker images | awk "\$1 == ${DOCKER_DB_NAME}" | awk '{print $1}')

  if [[ $NETWORK == '' ]]; then
    kpLabelText "Create docker network"
    docker network create kisphp
  fi

  if [[ $DATABASE_SERVICE == '' ]]; then
    kpLabelText "Build mysql docker image"
    docker build -t ${DOCKER_DB_NAME} ${KISPHP_KISPHP_DIRECTORY}/docker/mysql/
  fi

  kpLabelText "Run mysql service in docker detached"
  docker run \
    --rm \
    -d \
    --name kp-db \
    --net kisphp \
    -e MYSQL_ROOT_PASSWORD=kisphp \
    -e MYSQL_USER=kisphp \
    -e MYSQL_PASSWORD=kisphp \
    -e MYSQL_DATABASE=kisphp \
    -p 3306:3306 \
    -t ${DOCKER_DB_NAME}

  kpLabelText "Waiting 15 seconds for mysql to start"
  sleep 15

  kpLabelText "Run tests inside php docker container"
  docker run \
    --net kisphp \
    --link kp-db \
    --rm \
    -v $(pwd):/app \
    -w /app \
    -t registry.gitlab.com/marius-rizac/ci-registry/php7.4 \
    bash -c "make d t" 2> kphp-error.log

  kpLabelText "Stop mysql service"
  docker stop kp-db

#  echo $@
  kpLabelText "Tests finished"
}

codecept_help () {
  pre_help "codecept (cphp)" "Run kisphp project codeception tests inside docker containers from current directory"
}