#!/usr/bin/env bash

weather () {
    if [[ "${1}" != "" ]];then
        LOCATION="${1}"
    else
        LOCATION="Steglitz,Berlin"
    fi

    curl http://en.wttr.in/${LOCATION}
}

weather_help () {
    pre_help "weather (w)" "Show weather report in a location"
}




#https://www.datadoghq.com/blog/how-to-collect-and-graph-kubernetes-metrics/
#
#https://www.binarytides.com/linux-cpu-information/
#
#https://itnext.io/building-a-kubernetes-cluster-on-raspberry-pi-and-low-end-equipment-part-1-a768359fbba3
#
#https://aws.amazon.com/premiumsupport/knowledge-center/eks-worker-nodes-cluster/
#
#https://medium.com/@tarunprakash/5-things-you-need-know-to-add-worker-nodes-in-the-aws-eks-cluster-bfbcb9fa0c37
#
#https://aws.amazon.com/premiumsupport/knowledge-center/eks-multiple-node-groups-eksctl/
#
#https://sysdig.com/blog/kubernetes-autoscaler/
#
#https://www.alldaydevops.com/2019-live-schedule?utm_campaign=ADDO%202019&utm_source=hs_email&utm_medium=email&utm_content=79010246&_hsenc=p2ANqtz-8mB-bkBa2Xn496CJlVOVVrj2LNu5Np3fHysHShPlfhZix6v8Unq31YB7qbfhDIbKRpacP_Nc6MEiNELyjKD00k8eRt6A&_hsmi=79010246
#
#https://tech.travelaudience.com/armador-5fab64496e2a
#
#https://medium.com/@thomas.shaw78/bash-functions-as-a-service-b4033bc1ee97
#
#https://github.com/chubin/wttr.in
