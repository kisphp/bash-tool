#!/usr/bin/env bash

kisphp () {
    ARGUMENTS="$@"
    docker run \
        --rm \
        -v `pwd`:/project \
        -v ~/.ssh:/root/.ssh \
        -v ~/.composer:/root/.composer \
        -w /project \
        -t registry.gitlab.com/marius-rizac/ci-registry/php7.4:latest \
        /bin/bash -c "${ARGUMENTS}"
}

kisphp_help () {
    pre_help "kisphp (k)" "Run php 7.4 server"
}
