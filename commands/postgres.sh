#!/usr/bin/env bash

SERVICE_NAME='postgres_local'

postgres () {

    FIRST_PARAM=''

    if [[ $# -gt 0 ]]; then
        FIRST_PARAM="${1}"
    fi

    if [[ $(docker network ls -q -f name=${SERVICE_NAME}) == '' ]]; then
        echo "Creating network"
        docker network create $SERVICE_NAME
    fi

    if [[ "${FIRST_PARAM}" == 'admin' ]];then
        _postgres_run_adminer
    else
        _postgres_run_container
    fi
}

_postgres_run_container () {
    docker run \
        --rm \
        --name "${SERVICE_NAME}_db" \
        --network "${SERVICE_NAME}" \
        -e POSTGRES_PASSWORD=kisphp \
        -e POSTGRES_DB=kisphp \
        -p "5432:5432" \
        -t postgres:12
}

_postgres_run_adminer () {
    docker run \
        --rm \
        --network "${SERVICE_NAME}" \
        -e "PGADMIN_DEFAULT_EMAIL=admin@example.com" \
        -e "PGADMIN_DEFAULT_PASSWORD=kisphp" \
        -p "8880:80" \
        -t dpage/pgadmin4
}

postgres_help () {
    pre_help "postgres" "Run postgres inside a docker container"
    pre_help "postgres admin" "Run adminer to connect to postgres inside a docker container. View at localhost:8880. Connect with admin@example.com and pass: kisphp"
}
