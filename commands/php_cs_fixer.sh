#!/usr/bin/env bash

KISPHP_DM_PORT=8080

php_cs_fixer () {
    detect_linux_root

    ARGUMENTS="$@"
    docker run \
        --rm \
        -v ~/.ssh:/root/.ssh \
        -v ~/.composer:/root/.composer \
        -v `pwd`/src:/project/src \
        -v `pwd`/.php_cs:/project/.php_cs \
        -w /project \
        -t registry.gitlab.com/marius-rizac/ci-registry/php7.4:latest \
        sh -c "/bin/composer require friendsofphp/php-cs-fixer && vendor/bin/php-cs-fixer fix -v"
}

php_cs_fixer_help () {
    pre_help "php_cs_fixer [pcf]" "run php code fixer in src directory"
}
