#!/usr/bin/env bash

KISPHP_VERSION_LOCK_FILE='.kisphp_tool_last_update'

# update lock file will be updated for the next period
_upgrade_kp_timestamp () {
    # next five days
    TIMESTAMP=$(echo $(date +%s) + $KP_UPGRADE_DAYS | bc)

    echo "${TIMESTAMP}" > "${KISPHP_KISPHP_DIRECTORY}/logs/${KISPHP_VERSION_LOCK_FILE}"
}

# this method check if it is time to update the tool
# the update lock file will be updated for the next period
_kisphp_tool_should_upgrade () {
    if [[ $KP_UPGRADE_DAYS -eq 0 ]];then
        kp_log "upgrade disabled" "Kisphp tools"
        return 0
    fi

    if [[ ! -f "${KISPHP_KISPHP_DIRECTORY}/logs/${KISPHP_VERSION_LOCK_FILE}" ]]; then
        _upgrade_kp_timestamp
    fi

    NEXT_UPDATE=$(cat ${KISPHP_KISPHP_DIRECTORY}/logs/${KISPHP_VERSION_LOCK_FILE})
    if [[ $NEXT_UPDATE -lt $(date +%s) ]]; then
        echo -e "${INFO} [Kisphp Tool] Would you like to check for updates ? [Y|n] ${NC}"
        read line
        if [[ "$line" == Y* ]] || [[ "$line" == y* ]] || [ -z "$line" ]; then
            _upgrade_kp_tool
        fi
        _upgrade_kp_timestamp
    fi
}

# upgrade dotfiles tool
# this can be called directly without any confirmation
_upgrade_kp_tool () {
    kpInfoText "Upgrading kisphp tool request"
    CURRENT_DIRECTORY=$(pwd)
    cd "${KISPHP_KISPHP_DIRECTORY}"

    git fetch
    CURRENT_COMMIT_HASH=$(git rev-parse --verify HEAD)
    REMOTE_COMMIT_HASH=$(git rev-parse --verify origin/master)

    if [[ "$CURRENT_COMMIT_HASH" != "$REMOTE_COMMIT_HASH" ]];then
        git reset --hard
        git checkout master
        git pull --rebase origin master
    else
        kpSuccessText "Kisphp tool is already latest version"
    fi

    # update timestamp
    _upgrade_kp_timestamp

    cd $CURRENT_DIRECTORY
}

upgrade () {
    _upgrade_kp_tool
}

upgrade_help () {
    pre_help "upgrade (up)" "Run tool update to get the latest version"
}
